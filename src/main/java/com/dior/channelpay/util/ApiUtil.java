package com.dior.channelpay.util;

import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class ApiUtil {

    public static final String KEY_ALGORITHM = "RSA";
    public static final String SIGNATURE_ALGORITHM = "MD5withRSA";

    public ApiUtil() {
    }

    public static Map<String, Object> genKeyPair() throws Exception {
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
        keyPairGen.initialize(1024);
        KeyPair keyPair = keyPairGen.generateKeyPair();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        Map<String, Object> keyMap = new HashMap<>(2);
        keyMap.put("RSAPublicKey", publicKey);
        keyMap.put("RSAPrivateKey", privateKey);
        return keyMap;
    }

    /**
     * @Description: 私钥解密(密钥长度不同可以通用 ， 实测长度为1024和2048可用)
     * @MethodName:
     * @Date: 2018/6/10 20:13
     * @Author: Weiwei Zhan
     */
    public static String decryptByPrivateKey(String encryptedData, String privateKey) throws Exception {
        byte[] keyBytes = Base64.decode(privateKey);
        byte[] encryptedBytes = Base64.decode(encryptedData);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        PrivateKey privateK = keyFactory.generatePrivate(pkcs8KeySpec);

        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, privateK);
        int inputLen = encryptedBytes.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        /***获取私钥的长度***/
        int keySize = getPrivateKeySize(privateK, keyFactory);
        /**获取数据分块大小***/
        int blockSize = keySize / 8;

        for (int i = 0; inputLen - offSet > 0; offSet = i * blockSize) {
            byte[] cache;
            if (inputLen - offSet > blockSize) {
                cache = cipher.doFinal(encryptedBytes, offSet, blockSize);
            } else {
                cache = cipher.doFinal(encryptedBytes, offSet, inputLen - offSet);
            }

            out.write(cache, 0, cache.length);
            ++i;
        }

        byte[] decryptedData = out.toByteArray();
        out.close();
        return new String(decryptedData, "utf-8");
    }


    public static String encryptByPublicKey(String data, String publicKey) throws Exception {
        byte[] keyBytes = Base64.decode(publicKey);
        byte[] dataBytes = data.getBytes("utf-8");
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        Key publicK = keyFactory.generatePublic(x509KeySpec);
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(1, publicK);
        int inputLen = dataBytes.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;

        for (int i = 0; inputLen - offSet > 0; offSet = i * 116) {
            byte[] cache;
            if (inputLen - offSet > 116) {
                cache = cipher.doFinal(dataBytes, offSet, 116);
            } else {
                cache = cipher.doFinal(dataBytes, offSet, inputLen - offSet);
            }

            out.write(cache, 0, cache.length);
            ++i;
        }

        byte[] encryptedData = out.toByteArray();
        out.close();
        return Base64.encode(encryptedData).replaceAll("\n", "").replaceAll("\r\n", "").replaceAll("\r", "");
    }

    public static String signByPrivateKey(String data, String privateKey) throws Exception {
        byte[] keyBytes = Base64.decode(privateKey);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateK = keyFactory.generatePrivate(pkcs8KeySpec);
        Signature signature = Signature.getInstance("MD5withRSA");
        signature.initSign(privateK);
        signature.update(data.getBytes("utf-8"));
        return Base64.encode(signature.sign()).replaceAll("\n", "").replaceAll("\r\n", "").replaceAll("\r", "");
    }

    public static boolean validateSignByPublicKey(String paramStr, String publicKey, String signedData) throws Exception {
        byte[] keyBytes = Base64.decode(publicKey);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicK = keyFactory.generatePublic(keySpec);
        Signature signature = Signature.getInstance("MD5withRSA");
        signature.initVerify(publicK);
        signature.update(paramStr.getBytes("utf-8"));
        return signature.verify(Base64.decode(signedData));
    }

    /**
     * @Description: 获取私钥的长度
     * @MethodName:
     * @Date: 2018/6/10 20:13
     * @Author: Weiwei Zhan
     */
    private static int getPrivateKeySize(PrivateKey privateK, KeyFactory keyFactory) throws InvalidKeySpecException {

        RSAPrivateCrtKeySpec keySpec1 = keyFactory.getKeySpec(privateK, RSAPrivateCrtKeySpec.class);
        keySpec1.getModulus();
        int len = keySpec1.getModulus().toString(2).length();
        return len;

    }

    /**
     * @Description: 获取公钥的长度
     * @MethodName:
     * @Date: 2018/6/10 20:12
     * @Author: Weiwei Zhan
     */
    public static int getPublicKeySize(String publicKey) throws InvalidKeySpecException, NoSuchAlgorithmException {

        byte[] keyBytes = Base64.decode(publicKey);

        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        PublicKey publicK = keyFactory.generatePublic(keySpec);

        RSAPublicKeySpec keySpec1 = keyFactory.getKeySpec(publicK, RSAPublicKeySpec.class);
        keySpec1.getModulus();
        int len = keySpec1.getModulus().toString(2).length();
        return len;
    }


    public static int getPrivateKeySize(String privateKey) throws InvalidKeySpecException, NoSuchAlgorithmException {

        byte[] keyBytes = Base64.decode(privateKey);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        PrivateKey privateK = keyFactory.generatePrivate(pkcs8KeySpec);

        return getPrivateKeySize(privateK, keyFactory);
    }
    
    
    public static JSONObject parseToJson(String str) {

		try {
			Object data = JSON.parse(str);
			if (data instanceof JSONObject)
				return (JSONObject) data;

			return null;
		} catch (Exception e) {
			return null;
		}
	}
    
    
    public static String transferTimeFormat(String souceTime) {
    //"2021-03-30 17:26:55"
	String str1 = null;
	String sa[] = StringUtils.split(souceTime," ");
	if(sa != null && sa.length>0 && sa[0] != null && sa[1] != null) {
		str1 =sa[0].replace("-","")+sa[1].replace(":", "");
	}
	return str1;
}

}
