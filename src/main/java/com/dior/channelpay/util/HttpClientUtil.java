package com.dior.channelpay.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.dior.common.enums.ReturnCodeEnum;
import com.dior.common.enums.base.BaseEnum;
import com.dior.common.util.BaseUtil;
import com.dior.common.util.HttpClientUtils;
import com.dior.common.util.SSLClient;
import com.dior.common.util.HttpClientUtils.Data;

import lombok.extern.slf4j.Slf4j;

/**
	使用方法 
	
	@Resource
	HttpClientUtil httpClientUtil;
	
	...

	Data<String> receiveMsg = httpClientUtil.submitJson2(channelPayConfig.getProxyPayQueryBalanceUrl(), postStr);


 */


@Component
@Slf4j
public class HttpClientUtil {

	@Resource
	PoolingHttpClientConnectionManager poolingHttpClientConnectionManager;

	public CloseableHttpClient getCloseableHttpClient() {
		RequestConfig config = RequestConfig.custom()
				.setSocketTimeout(20000).setConnectTimeout(20000)
		        .setCookieSpec(CookieSpecs.IGNORE_COOKIES).build();
		CloseableHttpClient httpClient = HttpClients.custom()
				.setDefaultRequestConfig(config)
				.setConnectionManager(poolingHttpClientConnectionManager)
				.build();
		return httpClient;
	}

	//	public String doPost(String url, TreeMap<String, String> param) {
	//		// 创建Httpclient对象
	//		CloseableHttpClient httpClient = getCloseableHttpClient() ;
	//		CloseableHttpResponse response = null;
	//		String resultString = "";
	//		try {
	//			// 创建Http Post请求
	//			HttpPost httpPost = new HttpPost(url);
	//			// 创建参数列表
	//			if (param != null) {
	//				List<NameValuePair> paramList = new ArrayList<>();
	//				for (String key : param.keySet()) {
	//					paramList.add(new BasicNameValuePair(key, (String) param.get(key)));
	//				}
	//				// 模拟表单
	//				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(paramList);
	//				httpPost.setEntity(entity);
	//			}
	//			// 执行http请求
	//			response = httpClient.execute(httpPost);
	//			resultString = EntityUtils.toString(response.getEntity(), "utf-8");
	//		} catch (Exception e) {
	//			String err = BaseUtil.getTraceStack(e);
	//			System.out.format("doPost error:%s\n%s", e, err);
	//		} finally {
	//			try {
	//				if (response != null) {
	//					response.close();
	//				}
	//				httpClient.close();
	//			} catch (IOException e) {
	//				String err = BaseUtil.getTraceStack(e);
	//				System.out.format("doPost error:%s\n%s", e, err);
	//			}
	//		}
	//
	//		return resultString;
	//	}
	
	public Data<String> submitForm(String url, Map<String, String> param) {
		return submitForm(url,null, param);
	}
	
	/**
	 * 接口请求 form-post
	 * 
	 * @param url
	 * @param param
	 * @return
	 */
	public Data<String> submitForm(String url, Map<String, String> header, Map<String, String> param) {
		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
		if (header != null) {
			Set<Map.Entry<String, String>> set = header.entrySet();
			for (Map.Entry<String, String> e : set) {
				String k = e.getKey(), v = e.getValue();
				httpPost.addHeader(k, v);
			}
		}
		return submitForm(httpPost,param);
	}
	/**
	 * 接口请求 form-post
	 * 
	 * @param url
	 * @param param
	 * @return
	 */
	public Data<String> submitForm(HttpPost httpPost, Map<String, String> param) {
		// 创建Httpclient对象
		CloseableHttpClient httpClient = getCloseableHttpClient();
		CloseableHttpResponse response = null;
		String respMsg = "";
		Data<String> data = null;
		try {
			//HttpPost httpPost = new HttpPost(url);
			// String schema = httpPost.getURI().getScheme();
			// if (schema.equals("https")) {
			// 	httpClient = new SSLClient();
			// }
			// 创建参数列表
			if (param != null) {
				List<NameValuePair> paramList = new ArrayList<>();
				for (Map.Entry<String, String> entry : param.entrySet()) {
					paramList.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
				}
				// 模拟表单
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(paramList,"UTF-8");
				httpPost.setEntity(entity);
			}
			// 执行http请求
			response = httpClient.execute(httpPost);
			//resultString = EntityUtils.toString(response.getEntity(), "utf-8");
			StatusLine sl = response.getStatusLine();
			int sc = sl.getStatusCode();
			log.info("CookieSpec:{},[{}],httpStatus: {} \n", CookieSpecs.IGNORE_COOKIES,
					StringUtils.join(response.getAllHeaders(), ","), sc);

			if (sc >= 200 && sc < 300) {
				respMsg = EntityUtils.toString(response.getEntity());
			} else if (sc >= 300 && sc < 400) {
				Header hs[] = response.getHeaders("Location");
				if (hs != null && hs.length > 0) {
					respMsg = hs[0].getValue();
				}
			} else {
				int rc = -1;
				log.info("httpStatus:{}", sc);
				respMsg = EntityUtils.toString(response.getEntity());
			}
			data = new Data<String>(sc, respMsg);
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
			respMsg = HttpClientUtils.getRespJSONString(-1, ReturnCodeEnum.SERVICE_RESPONSE_DATA_ERROR);
			log.error("respMsg:{}\n{}\n{}", respMsg, ex, BaseUtil.getTraceStack(ex));
			data = new Data<String>(-1, respMsg);
		} finally {
			// 注意！！使用Connection Pool不可 httpClient.close();
			// try {
			// 	httpClient.close();
			// } catch (IOException e) {
			// 	e.printStackTrace();
			// }
		}

		return data;
	}

	public Data<String> submitJson(String url, String paramJsonStr) {
		HttpPost request = new HttpPost(url);
		return submitJson(request, paramJsonStr);
	}

	public Data<String> submitJson(HttpPost request, String paramJsonStr) {

		CloseableHttpClient httpClient = getCloseableHttpClient();
		HttpResponse response = null;
		String respMsg = null;
		Data<String> data = null;
		try {
			//HttpPost request = new HttpPost(url);
			//String schema = request.getURI().getScheme();
			// if (schema.equals("https")) {
			// 	httpClient = new SSLClient();
			// }
			StringEntity params = new StringEntity(paramJsonStr, "UTF-8");
			request.addHeader("content-type", "application/json;charset=UTF-8");
			request.setEntity(params);
			response = httpClient.execute(request);

			StatusLine sl = response.getStatusLine();
			int sc = sl.getStatusCode();
			log.info("CookieSpec:{},[{}],httpStatus: {} \n", CookieSpecs.IGNORE_COOKIES,
					StringUtils.join(response.getAllHeaders(), ","), sc);

			if (sc >= 200 && sc < 300) {
				respMsg = EntityUtils.toString(response.getEntity());
			} else if (sc >= 300 && sc < 400) {
				Header hs[] = response.getHeaders("Location");
				if (hs != null && hs.length > 0) {
					respMsg = hs[0].getValue();
				}
			} else {
				int rc = -1;
				log.info("httpStatus:{}", sc);
				respMsg = EntityUtils.toString(response.getEntity());
			}
			data = new Data<String>(sc, respMsg);
		} catch (Exception ex) {
			ex.printStackTrace();
			respMsg = HttpClientUtils.getRespJSONString(-1, ReturnCodeEnum.SERVICE_RESPONSE_DATA_ERROR);
			log.error("respMsg:{}\n{}\n{}", respMsg, ex, BaseUtil.getTraceStack(ex));
			data = new Data<String>(-1, respMsg);
		} finally {
			// 注意！！使用Connection Pool不可 httpClient.close();
			// try {
			// 	httpClient.close();
			// } catch (IOException e) {
			// 	e.printStackTrace();
			// }
		}

		return data;
	}

	public Data<String> submitGet2(String url,Map<String, String> header){
		HttpGet request = new HttpGet(url);
		return submitGet2(request , header);
	}
	
	public Data<String> submitGet2(HttpGet httpGet, Map<String, String> header) {
		System.out.println("--------------------------------------");

		// 创建默认的HttpClient实例.
		CloseableHttpClient httpClient = getCloseableHttpClient();//HttpClients.createDefault();

		int responseCode = 0;
		String result = "";
		try {
			//add header
			if (header != null) {
				Set<Map.Entry<String, String>> set = header.entrySet();
				for (Map.Entry<String, String> e : set) {
					String k = e.getKey(), v = e.getValue();
					httpGet.addHeader(k, v);
				}
			}
			
			CloseableHttpResponse response = httpClient.execute(httpGet);
			responseCode = response.getStatusLine().getStatusCode();
			ProtocolVersion protocolVersion = response.getStatusLine().getProtocolVersion();
			System.out.println("Status Code: " + responseCode);
			System.out.println("Protocol Version: " + protocolVersion.toString());

			// 状态码返回正常再处理返回内容
			if (responseCode == 200) {
				HttpEntity entity = response.getEntity();
//				InputStream inputStream = null;
				if (entity != null) {
					InputStream inputStream = entity.getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

					StringBuffer buffer = new StringBuffer();
					String line = "";
					while ((line = reader.readLine()) != null) {
						buffer.append(line).append("\r\n");
					}
					result = buffer.toString();
				} else {
					System.out.println("Entity is null!");
				}
			} else {
				result = getRespJSONString(responseCode, ReturnCodeEnum.INVALID_STATUS_CODE, "状态码不正确: " + responseCode);
				System.out.println("状态码不正确: " + responseCode);
				return new Data<String>(responseCode, result);
			}

			// release
			response.close();
//			httpClient.close();
		} catch (ClientProtocolException e) {
			responseCode = -1;
			result = getRespJSONString(responseCode, ReturnCodeEnum.SERVICE_RESPONSE_DATA_ERROR, e.getMessage());
			System.out.println("发生异常: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			responseCode = -1;
			result = getRespJSONString(responseCode, ReturnCodeEnum.SERVICE_RESPONSE_DATA_ERROR, e.getMessage());
			System.out.println("发生异常: " + e.getMessage());
			e.printStackTrace();
		} finally {
//			try {
//				if (httpClient != null)
//					httpClient.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
		}
		System.out.println("--------------------------------------");
		return new Data<String>(responseCode, result);
	}
	
	public String getRespJSONString(int status, BaseEnum be, String extErr) {
		return getRespJSONString(status, be.getCode(), be.getMsg() + ": " + extErr);
	}
	
	public String getRespJSONString(int status, String code, String msg) {
		Map map = new HashMap();
		map.put("status", status);
		map.put("respCode", code);
		map.put("respMsg", msg);
		String respMsg = JSON.toJSONString(map);
		return respMsg;
	}
	
}
