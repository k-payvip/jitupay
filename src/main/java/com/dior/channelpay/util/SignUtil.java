package com.dior.channelpay.util;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class SignUtil {
	
	/**
	 * 組成簽名原串
	 * 
	 * @param params
	 * @param merKey
	 * @return
	 */
	public static String getSource(Map<String, String> params, String merKey) {
		List<Map.Entry<String, String>> infoIds = new ArrayList<Map.Entry<String, String>>(
				params.entrySet());
		// 对所有传入参数按照字段名的 ASCII 码从小到大排序（字典序）
		Collections.sort(infoIds, new Comparator<Map.Entry<String, String>>() {
			public int compare(Map.Entry<String, String> o1,
					Map.Entry<String, String> o2) {
				return (o1.getKey()).toString().compareTo(o2.getKey());
			}
		});
		// 构造签名键值对的格式
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> item : infoIds) {
			if (item.getKey() != null || item.getKey() != "") {
				String key = item.getKey();
				if(item.getValue() == null) {
					continue;
				}
				String val = String.valueOf(item.getValue());
//				if (val != null && val.trim().length() > 0) {
				if (val != null) {
					sb.append(key + "=" + val + "&");
				}
			}
		}
//		sb.append("key=" + merKey);
		sb.deleteCharAt(sb.length()-1); //去掉最后一个&
		sb.append(merKey);

		return sb.toString();
	}
	
	/**
	 * 取得簽名後的字串(小寫)
	 * @param s
	 * @return
	 * @throws Exception 
	 */
	public final static String getSign(String s) throws Exception {
		char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

		byte[] btInput = s.getBytes("utf-8");
		// 获得MD5摘要算法的 MessageDigest 对象
		MessageDigest mdInst = MessageDigest.getInstance("MD5");
		// 使用指定的字节更新摘要
		mdInst.update(btInput);
		// 获得密文
		byte[] md = mdInst.digest();
		// 把密文转换成十六进制的字符串形式
		int j = md.length;
		char str[] = new char[j * 2];
		int k = 0;
		for (int i = 0; i < j; i++) {// 高低位分开处理
			byte byte0 = md[i];
			str[k++] = hexDigits[byte0 >>> 4 & 0xf];
			str[k++] = hexDigits[byte0 & 0xf];
		}
		
		return new String(str).toLowerCase();
	}
	
	/**
	 * 验签
	 * @throws Exception 
	 */
	public static boolean signVerify(String sign, Map<String, String> params, String key) throws Exception {

		String source = getSource(params, key);
		String verifySign = getSign(source);
		System.out.println("verifySource:" + source);
		System.out.println("verifySign:" + verifySign);
		System.out.println("reqSign:" + sign);
		
		if(!sign.equals(verifySign)) {
			return false;
		}
		return true;
	}
}
