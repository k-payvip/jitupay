package com.dior.channelpay.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.httpclient.methods.multipart.StringPart;

import com.alibaba.fastjson.JSONObject;

public class Test {
	public static void main(String[] arg) {
		Map<String ,String> queryData = new HashMap<String, String>();
		queryData.put("name", "Kenny");
		queryData.put("key", "123456");
		queryData.put("code", "Kenny123");
		queryData.put("test", "");
		
		
		Map<String, String> filterData = queryData.entrySet().stream()
				.filter(a -> a.getValue() != null && a.getValue() != "")
				.collect(Collectors.toMap(a -> a.getKey(), a -> a.getValue()));
		
		System.out.println("filterData: " + filterData.toString());
		
		Map<String, String> sortData = new TreeMap<>(
			(Comparator<String>) (a1,a2) -> a1.toUpperCase().compareTo(a2.toUpperCase())
		);
		sortData.putAll(filterData);
		
		System.out.println("sortData: " + sortData.toString());
		
		String queryString = sortData.entrySet().stream()
				 			.map(Object::toString).collect(Collectors.joining("&"));
		
		JSONObject jsonData = new JSONObject((Map)sortData);
		
		System.out.println("jsonData: " + jsonData.toJSONString());
		
//		System.out.println("queryString: " + queryString);
	}
}

