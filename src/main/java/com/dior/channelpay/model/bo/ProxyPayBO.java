package com.dior.channelpay.model.bo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProxyPayBO {
	/** 订单编号 */
	@NotBlank(message = "订单编号")
	@ApiModelProperty(value = "订单编号", required = true, position = 1)
	String orderNo;
	
	/** 交易类型 */
	@ApiModelProperty(value = "交易类型", required = true, position = 2)
	String transactionType;
	
	/** 交易金额 */
	@NotBlank(message = "交易金额")
	@Pattern(regexp = "^(([1-9][0-9]*)|(([0]\\.\\d{0,2}|[1-9][0-9]*\\.\\d{0,2})))$", message = "交易金额仅能为数字")
	@ApiModelProperty(value = "交易金额", required = false, position = 3)
	String transactionAmount;
	
    /** 收款帐号  */
	@ApiModelProperty(value = "收款帐号", required = false, position = 4)
    private String receiptAccountNumber;

    /** 收款户名  */
	@ApiModelProperty(value = "收款户名", required = false, position = 5)
    private String receiptAccountName;
	
	/** 银行卡归属 省份 */
	@ApiModelProperty(value = "银行卡归属 省份", required = false, position = 6)
    private String receiptCardProvince;
	
	/** 银行卡归属 城市 */
	@ApiModelProperty(value = "银行卡归属 城市", required = false, position = 7)
	private String receiptCardCity;
	
	/** 银行卡归属 区域/县 */
	@ApiModelProperty(value = "银行卡归属 区域/县", required = false, position = 8)
	private String receiptCardArea;
	
	/** 银行卡归属 支行名称 */
	@ApiModelProperty(value = "银行卡归属 支行名称", required = false, position = 9)
	private String receiptCardBranch;
	
	/** 银行卡代码 */
	@ApiModelProperty(value = "银行卡代码", required = false, position = 10)
	private String receiptCardBankCode;
	
	/** 银行卡银行名称 */
	@ApiModelProperty(value = "银行卡归属 银行名称", required = false, position = 11)
	private String receiptCardBankName;
	
	/** 创建人 */
	@ApiModelProperty(value = "创建人", required = false, position = 12)
	private String createdBy;
	
	/** 创建时间 */
	@ApiModelProperty(value = "创建时间", required = false, position = 13)
	private String createdAt;
	
	/** Flag下發轉代付*/
	@ApiModelProperty(value = "下發轉代付", required = false, position = 14)
	private boolean isWithdrawToProxy;
}