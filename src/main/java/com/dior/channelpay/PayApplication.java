package com.dior.channelpay;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.dior.channelpay.config.ChannelPayConfig;
import com.dior.common.model.vo.BankChannelMapVO;
import com.dior.common.util.BaseUtil;

import lombok.extern.slf4j.Slf4j;

@EnableTransactionManagement(proxyTargetClass = true)
@SpringBootApplication(scanBasePackages = "com.dior")
@MapperScan(value= {"com.dior.channelpay.storage.mapper","com.dior.common.storage.mapper"})
@Slf4j
public class PayApplication implements CommandLineRunner{

	@Value("${deploy.releaseBranch}")
	private String releaseBranch;
	
	@Value("${deploy.releaseDate}")
	private String releaseDate;
	

	@Autowired
	private ChannelPayConfig channelPayConfig;

	public static void main(String[] args) {
		SpringApplication.run(PayApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("deploy releaseBranch:{} releaseDate:{}", releaseBranch, releaseDate);
		log.info("channelcommon releaseBranch:{} releaseDate:{}", BaseUtil.ReleaseBranch, BaseUtil.ReleaseDate);
		String project = channelPayConfig.getProject();
		String projectName = channelPayConfig.getProjectName();
		String channelName = channelPayConfig.getChannelName();
		String channelCoding = channelPayConfig.getChannelCoding();
		String merId = channelPayConfig.getMerId();
		String merKey = channelPayConfig.getMerKey();
		String pu = channelPayConfig.getPayUrl();
		String ru = channelPayConfig.getProxyPayUrl();
		String server = channelPayConfig.getServer();
		int port = channelPayConfig.getPort();
		String cpu = channelPayConfig.getCallbackPayChannelpay();
		String cru = channelPayConfig.getCallbackProxypayChannelpay();
		Map<String, String> vo = channelPayConfig.getBankChannelMap();
		
		Map<String, String> map = channelPayConfig.getPayTypeMap();
		log.info("Project:{}-{}-{}-{},Mer:{}={},url:{}-{},map:{}\nserver:{},port:{},pay:{},proxy:{}", project,projectName,channelName,channelCoding,merId,merKey,pu,ru,map,server,port,cpu,cru);
		log.info("BankChannelMapVO:{}",vo);
	}
}
