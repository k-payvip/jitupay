package com.dior.channelpay.config;

import java.time.Duration;
import java.util.Map;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

//@ConfigurationProperties(prefix = "dior.redis")
//@Configuration
//@Slf4j
//@Data
@Configuration
@EnableCaching
public class RedisConfig {
	
	@Value("${spring.redis.database}")
	private Integer database;
	@Value("${spring.redis.host}")
	private String host;
	@Value("${spring.redis.port}")
	private Integer port;
	@Value("${spring.redis.lettuce.pool.max-idle}")
	private int maxIdle;
	@Value("${spring.redis.lettuce.pool.min-idle}")
	private int minIdle;
	@Value("${spring.redis.lettuce.pool.max-active}")
	private int maxTotal;
	@Value("${spring.redis.lettuce.pool.max-wait}")
	private int maxWaitMillis;
	@Value("${spring.redis.password}")
	private String password;
	@Value("${spring.redis.timeout}")
	private long timeout;

	@Bean
	public LettuceConnectionFactory lettuceConnectionFactory() {
		LettuceClientConfiguration clientConfig = LettucePoolingClientConfiguration.builder()
				.commandTimeout(Duration.ofMillis(timeout)).poolConfig(genericObjectPoolConfig()).build();
		LettuceConnectionFactory factory = new LettuceConnectionFactory(rediStandaloneConfiguration(), clientConfig);
		return factory;

	}

	public GenericObjectPoolConfig genericObjectPoolConfig() {
		GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
		genericObjectPoolConfig.setMaxTotal(maxTotal);
		genericObjectPoolConfig.setMaxWaitMillis(maxWaitMillis);
		genericObjectPoolConfig.setMaxIdle(maxIdle);
		genericObjectPoolConfig.setMinIdle(minIdle);
		return genericObjectPoolConfig;
	}

	public RedisStandaloneConfiguration rediStandaloneConfiguration() {
		RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
		redisStandaloneConfiguration.setDatabase(database == null ? 0 : database);
		redisStandaloneConfiguration.setHostName(host);
		redisStandaloneConfiguration.setPort(port);
		redisStandaloneConfiguration.setPassword(password);
		return redisStandaloneConfiguration;
	}

	@Bean
	public RedisTemplate<?, ?> redisTemplate() {
		// 設定序列化
		// 使用Jackson2JsonRedisSerializer來序列化和反序列化redis的value值
	        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
	        ObjectMapper om = new ObjectMapper();
	        //指定要序列化的域，field,get和set,以及修飾符範圍，ANY是都有包括private和public
	        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
	        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
	        jackson2JsonRedisSerializer.setObjectMapper(om);
		
	        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
		
	        RedisTemplate<?, ?> redisTemplate = new RedisTemplate();
	        redisTemplate.setConnectionFactory(lettuceConnectionFactory());
	        
	        redisTemplate.setKeySerializer(stringRedisSerializer);
	        redisTemplate.setValueSerializer(stringRedisSerializer);
	        redisTemplate.setHashKeySerializer(stringRedisSerializer);
	        redisTemplate.setHashValueSerializer(stringRedisSerializer);
	        redisTemplate.afterPropertiesSet();
	        return redisTemplate;
//		return new StringRedisTemplate(lettuceConnectionFactory());
	}
	
/*
    private String host;

    private int port;

    private String password;

//    @Bean
//    public JedisPoolConfig getRedisConfig() {
//        JedisPoolConfig config = new JedisPoolConfig();
//        config.setMaxIdle(50);
//        config.setMinIdle(1);
//        config.setMaxWaitMillis(-1);    //等待可用连线时间的最大时间，单位毫秒，预设值-1(永不超时)
//        config.setTestOnBorrow(true);   //是否提前执行validate操作
//        return config;
//    }

    @Scope(scopeName = "prototype")
    public JedisConnectionFactory jedisConnectionFactory() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setHostName(host);
        redisStandaloneConfiguration.setPort(port);
        redisStandaloneConfiguration.setDatabase(1);

        JedisClientConfiguration.JedisClientConfigurationBuilder jedisClientConfiguration = JedisClientConfiguration.builder();
        jedisClientConfiguration.connectTimeout(Duration.ofMillis(10000));//  connection timeout
        JedisClientConfiguration jcc = jedisClientConfiguration.build();
        JedisConnectionFactory factory = new JedisConnectionFactory(redisStandaloneConfiguration, jcc);
        return factory;
    }

    @Bean	//(name = "msm-redis")
    public RedisTemplate<String, Map<String, Object>> redisTemplate() {
        final RedisTemplate<String, Map<String, Object>> template = new RedisTemplate<String, Map<String, Object>>();
        JedisConnectionFactory jedisConnectionFactory = jedisConnectionFactory();
        template.setConnectionFactory(jedisConnectionFactory);

        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        template.setKeySerializer(stringRedisSerializer);
        template.setHashKeySerializer(stringRedisSerializer);
        template.setHashValueSerializer(stringRedisSerializer);
        return template;
    }
    */
}
