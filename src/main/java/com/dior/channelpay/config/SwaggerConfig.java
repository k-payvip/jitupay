package com.dior.channelpay.config;

import com.google.common.net.HttpHeaders;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;

/**
 * @author JamesChung
 * Description: Swagger UI Config
 * Date: 2019-08-13
 */
@Configuration
@EnableSwagger2
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "swagger.config")
@Component
@Data
public class SwaggerConfig implements WebMvcConfigurer {

    private String basePackage;
    private String title;
    private String description;
    private String version;
    private String enable;

    @Bean
    public Docket appConfig() {
    	boolean enabled = enable == null || !enable.equalsIgnoreCase("false");
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(appInfo())
                .enable(enabled)
                .forCodeGeneration(true)
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(Arrays.asList(apiKey()));
    }

    private ApiInfo appInfo() {
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
                .version(version)
                .build();
    }

    private ApiKey apiKey() {
        return new ApiKey(HttpHeaders.AUTHORIZATION, HttpHeaders.AUTHORIZATION, "header");
    }

}
