package com.dior.channelpay.config;

import java.util.Set;

import javax.annotation.Resource;

import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.pool.PoolStats;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ScheduledTasks {

	@Resource
	PoolingHttpClientConnectionManager poolingHttpClientConnectionManager;

	/**
	 * 每5分鐘
	 */
	@Scheduled(cron = "0 0/1 * * * *")
	public void httpPoolStats() {
		try {
			if (poolingHttpClientConnectionManager == null)
				return;
			StringBuilder sb = new StringBuilder();
			Set<HttpRoute> routes = poolingHttpClientConnectionManager.getRoutes();
			routes.forEach(e -> {
				PoolStats stats = poolingHttpClientConnectionManager.getStats(e);
				sb.append("Per route:" + routes.toString() + stats.toString() + ", ");
			});
			// 获取所有路由的连接池状态
			PoolStats totalStats = poolingHttpClientConnectionManager.getTotalStats();
			log.info("PoolStats {}, Total :{}", sb, totalStats.toString());
		} catch (Throwable e) {
		}
	}
}