package com.dior.channelpay.config;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.ManagedHttpClientConnectionFactory;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableTransactionManagement
public class RestTemplateConfig {
	
	public static final int TIME_OUT = 10 * 1000;

	@Bean
	public RestTemplate restTemplate(){
		RestTemplate restTemplate = new RestTemplate();
		SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
		rf.setConnectTimeout(TIME_OUT);
		rf.setReadTimeout(TIME_OUT);
		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new LoggingRequestInterceptor());
		restTemplate.setInterceptors(interceptors);

		return restTemplate;
	}
	
//	@Bean
//	public RestTemplateEx restTemplateEx(){
//		return new RestTemplateEx();
//	}

	@Bean("defaultRestTemplateProxy")
	public RestTemplate restTemplateProxy(){
		RestTemplate restTemplate = new RestTemplate();
		SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
		rf.setConnectTimeout(TIME_OUT);
		rf.setReadTimeout(TIME_OUT);
		return restTemplate;
	}

	@Bean("ipTemplateProxy")
	public RestTemplate restTemplateProxyIP(){
		RestTemplate restTemplate = new RestTemplate();
		SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
		rf.setConnectTimeout(TIME_OUT);
		rf.setReadTimeout(TIME_OUT);
		return restTemplate;
	}
	@Bean("sslRestTemplate")
	public RestTemplate restTemplateSSL() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		SSLContext sslContext = getSSLContext();

		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
//		PoolingHttpClientConnectionManager pm = poolingHttpClientConnectionManager == null ? poolingHttpsClientConnectionManager() 
//				: poolingHttpClientConnectionManager;
		PoolingHttpClientConnectionManager pm = poolingHttpsClientConnectionManager();
		CloseableHttpClient httpClient = HttpClients.custom()
				.setSSLSocketFactory(csf)
				.setConnectionManager(pm)
				.build();

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(httpClient);
		requestFactory.setConnectTimeout(TIME_OUT);
		requestFactory.setConnectionRequestTimeout(TIME_OUT);
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		org.springframework.web.client.DefaultResponseErrorHandler handler = new org.springframework.web.client.DefaultResponseErrorHandler() {
			public boolean hasError(ClientHttpResponse response) throws IOException {
				int rawStatusCode = response.getRawStatusCode();
				HttpStatus statusCode = HttpStatus.resolve(rawStatusCode);
				return false;
				//return (statusCode != null ? hasError(statusCode) : hasError(rawStatusCode));
			}
			
		};
		restTemplate.setErrorHandler(handler);
		return restTemplate;
	}
	SSLContext getSSLContext() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

		SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
				.loadTrustMaterial(null, acceptingTrustStrategy)
				.build();
		return sslContext;
	}
//	@Resource
//	PoolingHttpClientConnectionManager poolingHttpClientConnectionManager;
	@Bean
	public PoolingHttpClientConnectionManager poolingHttpsClientConnectionManager() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		SSLContext sslContext = getSSLContext();
		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
		Registry<ConnectionSocketFactory> factory = RegistryBuilder.<ConnectionSocketFactory>create().
				register("http", PlainConnectionSocketFactory.getSocketFactory()).
				register("https", csf).
				build();
		PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager(factory
				,ManagedHttpClientConnectionFactory.INSTANCE, null, null, -1, TimeUnit.MILLISECONDS);
		manager.setMaxTotal(TIME_OUT);
		manager.setDefaultMaxPerRoute(TIME_OUT);
		return manager;
	}
	@Bean("monitorRestTemplate")
	public RestTemplate monitorTemplate(){
		RestTemplate restTemplate = new RestTemplate();
		SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
		rf.setConnectTimeout(TIME_OUT);
		rf.setReadTimeout(TIME_OUT);
		return restTemplate;
	}

}
