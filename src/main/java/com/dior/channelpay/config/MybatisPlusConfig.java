package com.dior.channelpay.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@MapperScan("com.dior.channelpay.storage.mapper")
public class MybatisPlusConfig {

    private static final String DB_PREFIX = "spring.datasource";

    @Bean
    public PerformanceInterceptor performanceInterceptor() {
        PerformanceInterceptor performanceInterceptor = new PerformanceInterceptor();
        //格式化sql语句
        return performanceInterceptor;
    }

    /**
	     * 分页插件
	 */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    //读取相关的属性配置
    @ConfigurationProperties(prefix = DB_PREFIX)
    @Bean
    public DataSource dataSource() {
        return new DruidDataSource();
    }
}