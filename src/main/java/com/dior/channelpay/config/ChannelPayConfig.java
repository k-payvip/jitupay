package com.dior.channelpay.config;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.dior.common.util.ChannelInfoProvider;

import lombok.extern.slf4j.Slf4j;

@Component
//@ConfigurationProperties(prefix = "channelpay")
//@Data
@Slf4j
public class ChannelPayConfig extends ChannelInfoProvider {

    @Autowired
    RedisTemplate<String, String> redisTemplate;

    @Autowired
	private ChannelPayCommonConfig channelPayCommonConfig;
    
	@Value("${channelpay.server}")
	private String server;
    
	@Value("${server.port}")
	private int port;
    
	@Override
	public String getProject() {
		return "jitupay";
	}

	@Override
	public RedisTemplate<String, String> getRedisTemplate() {
		log.info("projectName :{}, redisTemplate :{}" ,getProject(),redisTemplate);
		return redisTemplate;
	}

//	@Override
//	public int getPort() {
//		return port;
//	}

	@Override
	public String getServer() {
		return channelPayCommonConfig.getServer();
	}

	public Map<String, String> getBankCodeMap() {
		return channelPayCommonConfig.getBankCodeMap();
	}

}

/***
//	private Map<String, String> bankCodeMap; //銀行代碼對應Map
	
	private Map<String, String> payTypeMap; //銀行代碼對應Map
	
	private String callbackProxypayChannelpay; //給proxypay渠道的回調地址
	
	private String callbackProxypayDiorpayment; //將回調結果回傳diorpayment的回調地址
	
	private String callbackPayChannelpay; //給proxypay的渠道回調地址
	
	private String callbackPayDiorportal; //給diorportal的渠道回調地址
	
	private String merId; //商戶ID
	
	private String merKey; //商戶Key
	
	private String payMerId; //支付商戶ID
	
	private String payMerKey; //支付商戶Key
	
	private String payUrl; 		// 付款地址
    private String payQueryUrl; // 付款查询地址
    private String proxyPayUrl; // 代付地址
    private String proxyPayQueryUrl;	// 代付查询地址
    private String proxyPayQueryBalanceUrl; // 代付查询余额地址
    private String defaultReturnUrl;	// 支付跳轉位置
    
    private String channelCoding;	// 渠道編碼
    private int queryProxyBalance;	// 渠道代付馀额查詢 每隔多少微秒查詢一次 ( <= 0 表示不啟動查詢）
    
    private String[] whiteList; //渠道回调白名单
 */