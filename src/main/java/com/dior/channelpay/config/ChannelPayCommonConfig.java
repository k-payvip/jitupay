package com.dior.channelpay.config;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "channelpay")
@Data
public class ChannelPayCommonConfig {
	
	private String server;
    
	private int port;
    
	private Map<String, String> bankCodeMap; //銀行代碼對應Map
}
