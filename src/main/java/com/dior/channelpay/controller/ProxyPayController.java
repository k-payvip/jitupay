package com.dior.channelpay.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.http.client.methods.HttpPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dior.channelpay.config.ChannelPayConfig;
import com.dior.channelpay.model.bo.ProxyPayBO;
import com.dior.channelpay.util.ApiUtil;
import com.dior.channelpay.util.HttpClientUtil;
import com.dior.channelpay.util.SignUtil;
import com.dior.common.enums.CommonEnum;
import com.dior.common.enums.ReturnCodeEnum;
import com.dior.common.enums.base.BaseEnum;
import com.dior.common.enums.base.RespData;
import com.dior.common.enums.base.RespStatusEnum;
import com.dior.common.exception.DiorException;
import com.dior.common.model.bo.QueryOrderBO;
import com.dior.common.model.vo.ChannelBalanceVO;
import com.dior.common.model.vo.ProxyPayCallbackVO;
import com.dior.common.model.vo.ProxyPayChannelReplyVO;
import com.dior.common.model.vo.ProxyPayQueryReplyVO;
import com.dior.common.model.vo.QueryBalanceVO;
import com.dior.common.model.vo.QueryOrderChannelReplyVO;
import com.dior.common.service.ChannelDataService;
import com.dior.common.storage.entity.WithdrawOrder;
import com.dior.common.storage.mapper.WithdrawOrderMapper;
import com.dior.common.util.BaseUtil;
import com.dior.common.util.EncryptUtil;
import com.dior.common.util.HttpClientUtils;
import com.dior.common.util.TdDateUtil;
import com.dior.common.util.HttpClientUtils.Data;
import com.google.common.base.Optional;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("jitupay/api")
@Api(value = "串接 proxypay 渠道 API")
@Slf4j
public class ProxyPayController {

	/********************************************************************************
	 * declare properties
	 ********************************************************************************/
	@Value("${proxypay.key}")
	private String proxyPayKey;

	@Value("${proxypay.encryptKey}")
	private String proxyPayEncryptKey;
	
	@Value("${spring.application.name}")
	private String channelName;

	@Autowired
	private ChannelPayConfig channelPayConfig;
	
	@Resource
	private ChannelDataService channelDataService;

	@Resource 
	private HttpClientUtil httpClientUtil;
	/********************************************************************************
	 * common methods
	 ********************************************************************************/
	boolean authCheck(String sign) {
		log.info("Header 验签 = {}，加密Key：{}，金鑰：{}", sign, proxyPayEncryptKey, proxyPayKey);
		/** 目前期望加密机制： proxyKey+YYYYMMDDhhmmss， 解密后时间不超过5分钟内 */
		boolean result = EncryptUtil.microServiceDecrypt(sign, proxyPayEncryptKey, proxyPayKey);
		return result;
	}
	<T> RespData<T> prepareException(Throwable e){
		return prepareException(e, CommonEnum.CONNECT_SERVICE_FAILURE);
	}
	<T> RespData<T> prepareException(Throwable e,BaseEnum be){
		log.info("连接渠道出错:{}", e.toString());
		log.info("连接渠道出错LogStack:{}", BaseUtil.getLogStack(e));
		if(e instanceof DiorException) 
			be = ((DiorException)e).getBaseEnum();
		
		String msg = e.getMessage();
		if(msg.length()>500) {
			msg = msg.substring(0, 500)+" ...";
		}
		RespData<T> respData = new RespData<T>();
		respData.setStatus(RespStatusEnum.FAILURE.status);
		respData.setCode(be.getCode()).setMsg(msg);
		
		return respData;
	}
	/********************************************************************************
	 * 1.串接 proxypay 代付下單渠道
	 ********************************************************************************/
	@ApiOperation(value = "串接 proxypay 代付下單渠道", notes = "串接 proxypay 代付下單渠道")
	@PostMapping(path = "/proxy-pay", produces = { "application/json" })
	public RespData<ProxyPayChannelReplyVO> pay(@RequestHeader(name = "authentication-proxykey") String sign,
			@Valid @RequestBody ProxyPayBO proxyPayBO) {
		RespData<ProxyPayChannelReplyVO> resp = null;
		try {
			log.info("代付订单channelName:{},ChannelPayOrder:{}",channelName, proxyPayBO);
			// 1.microService check authentication-proxykey
			if (!authCheck(sign)) 
				return new RespData<>(ReturnCodeEnum.INVALID_SIGN); // 驗簽錯誤
			
			// 2. 20220117 下發轉代付 withdraw log() 
			if(proxyPayBO != null && proxyPayBO.isWithdrawToProxy())
				insertWithdrawRecord(proxyPayBO);
						
			// 2.prepare input map
			String receiveMsg = proxyPayOrderInputAndPost(proxyPayBO);
			log.info("代付订单 receiveMsg:{}", receiveMsg);
			resp = prepareProxyPayReply(receiveMsg);
		} catch (Throwable e) {
			resp = prepareException(e);
		}
		return resp;
	}
	/********************************************************************************
	 * 2.串接proxypay查询代付订单
	 ********************************************************************************/
	@ApiOperation(value = "串接proxypay查询代付订单", notes = "串接proxypay查询代付订单")
	@PostMapping(path = "/proxy-pay-query", produces = { "application/json" })
	public RespData<ProxyPayQueryReplyVO> orderPayQuery(@RequestHeader(name = "authentication-proxykey") String sign, 
		@Valid @RequestBody QueryOrderBO queryOrderBO) {
		RespData<ProxyPayQueryReplyVO> resp = null;
		try {
			// 1.microService check authentication-proxykey
			log.info("代付订单channelName:{},查询payBO:{}",channelName, queryOrderBO);
			if (!authCheck(sign)) 
				return new RespData<>(ReturnCodeEnum.INVALID_SIGN); // 驗簽錯誤
			// 2.prepare input map
			String receiveMsg = proxyPayQueryInputAndPost(queryOrderBO);
			log.info("代付订单查询 receiveMsg:{}", receiveMsg);
			resp = prepareProxyPayQueryReply(receiveMsg);
		} catch (Exception e) {
			resp = prepareException(e);
		}
		return resp;
	}
	/********************************************************************************
	 * 3.串接proxypay代付渠道回調
	 ********************************************************************************/
	@ApiOperation(value = "串接proxypay代付渠道回调", notes = "串接proxypay代付渠道回调")
//	@PostMapping(path = "/proxy-pay-call-back", produces = { "application/json" })
//	public String payCallBackAction(@RequestBody Map<String, String> params,HttpServletRequest request) {
//	@PostMapping(path = "/proxy-pay-call-back", consumes  = { "multipart/form-data" })
//	public String payCallBackAction(@RequestPart("sh_order") String sh_order ,
//			@RequestPart("pt_order") String pt_order,
//			@RequestPart("money") String money ,
//			@RequestPart("time") String time ,
//			@RequestPart("status") String status ,
//			@RequestPart("remark") String remark ,
//			@RequestPart("sign") String sign ,HttpServletRequest request) {
	@PostMapping(path = "/proxy-pay-call-back", produces = { "application/x-www-form-urlencoded" })
	public String payCallBackAction(@RequestParam Map<String, String> params, HttpServletRequest request) {
		try {
			String address = request.getRemoteAddr();
			log.info("串接channelName:{},代付渠道回调:{},address:{}",channelName, params,address);
			
			//检查渠道回调的白名单
			BaseUtil.checkWhiteList(request, channelPayConfig.getWhiteList());
			
			//TODO 1.验签
			String reqSign = params.get("sign"); //簽名
			params.remove("sign");
			if(!SignUtil.signVerify(reqSign, params, channelPayConfig.getMerKey())) {
				log.error("验签出错-sign:{}, paramMap:{}, key:{}", reqSign, params, channelPayConfig.getMerKey());
				return "";
			}
			
			//TODO 2.准备返回渠道通知的訂單相關資訊
			String orderNo = params.get("orderNo"); //商戶訂單號
//			String serializeId = params.get("serializeId"); //平台交易单号
			String amount = params.get("amount"); //代付金额
			String retcode = params.get("retcode"); //代付订单状态:0000-成功 0001-审核失败 0002-下发失败
			
			ProxyPayCallbackVO callbackVO = new ProxyPayCallbackVO();
			callbackVO.setProxyPayOrderNo(orderNo); //代付訂單號
//			callbackVO.setChannelOrderNo(serializeId); //渠道訂單號
			
			String orderStatus = "1"; //处理中
			if("0000".equals(retcode)) { //成功
				orderStatus = "2";
			}else if("0001,0002".indexOf(retcode) > -1) { //失败
				orderStatus = "3";
			}
			callbackVO.setAmount(BaseUtil.getDecimal(amount));
			callbackVO.setChannelResultAt(TdDateUtil.getDateTime()); //回调时间
			callbackVO.setChannelResultStatus(orderStatus); //回调状态
			
			//3.微服務加密 and submit
			String callBackAddress = channelPayConfig.getCallbackProxypayDiorpayment();
			HttpPost httpPost = new HttpPost(callBackAddress);
			httpPost.addHeader("authentication-proxykey", EncryptUtil.microServiceEncrypt(proxyPayKey, proxyPayEncryptKey));
			String callBackJson = JSON.toJSONString(callbackVO);
			log.info("回調到diorPayment url:{}, json:{}", callBackAddress, callBackJson);
			String respMsg = HttpClientUtils.submitJson(httpPost, callBackJson);
			log.info("回调后资讯:{}",respMsg);
		} catch (Exception e) {
			log.info("串接channelName:{},proxypay代付渠道回調出错:{}",channelName, BaseUtil.getLogStack(e));
		}
		return "success";
	}

	@ApiOperation(value = "串接proxypay查询代付余额", notes = "串接proxypay查询代付余额")
	@PostMapping(path = "/proxy-pay-query-balance-internal", produces = { "application/json" })
	public ChannelBalanceVO queryBalanceInternal(@RequestHeader(name = "authentication-proxykey") String sign) {
		ChannelBalanceVO resp = new ChannelBalanceVO();
		
		RespData<QueryBalanceVO> res = queryBalance(sign);
		QueryBalanceVO data = res.getData();
		resp.setErrorCode(res.getCode()).setErrorMsg(res.getMsg());
		if(data != null) {
			resp.setProxyPayBalance(data.getDfBalance());
		}
		
		return resp;
	}
	RespData<QueryBalanceVO> queryBalance(String sign){
		RespData<QueryBalanceVO> resp = null;
		
		try {
			// microService check authentication-proxykey
			if (!authCheck(sign)) 
				return new RespData<>(); // 驗簽錯誤

			//TODO 1.prepare input
			String merchant = channelPayConfig.getMerId(); //商户ID
			
			Map<String, String> paramMap = new HashMap<>();
			paramMap.put("merchant", merchant);
			
			//TODO 2.prepare sign
			String source = SignUtil.getSource(paramMap, channelPayConfig.getMerKey());
			String reqSign = SignUtil.getSign(source);
			log.info("加签原串:{},加签后字串:{}",source, reqSign);
			
			//TODO 3.submit and get receive message
			JSONObject paramJo = new JSONObject((Map)paramMap);
			JSONObject jo = new JSONObject();
			jo.put("method", "getBalance");
			jo.put("params", paramJo);
			jo.put("sign", reqSign);
			
			String js = jo.toJSONString();
			log.info("查询余额请求参数:{}", js);
			Data<String> receiveMsg = httpClientUtil.submitJson(channelPayConfig.getProxyPayQueryBalanceUrl(), js);
			log.info("receiveMsg: rc:{} data:{}" ,receiveMsg.rc,receiveMsg.data);
			if(receiveMsg.rc == -1) {
				throw new DiorException(ReturnCodeEnum.SERVICE_RESPONSE_DATA_ERROR).setMsg(receiveMsg.data);
			}else if(receiveMsg.rc != 200) {
				throw new DiorException(ReturnCodeEnum.INVALID_STATUS_CODE).setMsg(receiveMsg.rc+ " : " + receiveMsg.data);
			}
			
			//TODO 4.处理渠道返回错误讯息
			JSONObject json = ApiUtil.parseToJson(receiveMsg.data);// 返回JSON字串
			if(json == null) {
				resp = new RespData<QueryBalanceVO>(RespStatusEnum.FAILURE, CommonEnum.CHANNEL_REPLY_ERROR).setExternalMessage(receiveMsg.data);
			}
			JSONObject errorJo = JSON.parseObject(json.getString("error"));
			int code = errorJo.getInteger("code");	// 200:成功
			String message = errorJo.getString("message");
			if(code != 200) {
				log.info("余额查询 error code:{}, error:{}", code, message);
				resp = new RespData<QueryBalanceVO>(RespStatusEnum.FAILURE, CommonEnum.CHANNEL_REPLY_ERROR).setExternalMessage(message);
			}
			
			//TODO 5.返回代付余额
			QueryBalanceVO replyVO = new QueryBalanceVO();
			replyVO.setDfBalance(json.getBigDecimal("result"));
			
			return new RespData<>(RespStatusEnum.SUCCESS, CommonEnum.SUCCESS, replyVO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp = prepareException(e);
		}
		
		return resp;
	}
	
	/********************************************************************************
	 * 4.串接proxypay查询代付余额
	 ********************************************************************************/
	@ApiOperation(value = "串接proxypay查询代付余额", notes = "串接proxypay查询代付余额")
	@PostMapping(path = "/proxy-pay-query-balance", produces = { "application/json" })
	public RespData<QueryBalanceVO> proxyPayQueryBalance(@RequestHeader(name = "authentication-proxykey") String sign) {
		RespData<QueryBalanceVO> resp = null;
		
		try {
			resp = queryBalance(sign);
			if(resp.getData()!=null && resp.getData().getDfBalance()!=null) {
				BigDecimal DfBalance = resp.getData().getDfBalance();
				//TODO 6.更新代付餘額
				QueryBalanceVO replyVO = resp.getData();
				String channelCoding = channelPayConfig.getChannelCoding();
				log.info("更新渠道代付馀额--渠道:{}, 馀额:{}", channelCoding, DfBalance);
				channelDataService.updateChannelBalance(channelCoding, DfBalance.setScale(2,BigDecimal.ROUND_DOWN));
				return new RespData<>(RespStatusEnum.SUCCESS, CommonEnum.SUCCESS, replyVO);
			}else {
				log.error("查询余额返回错误:{}",resp);
				return new RespData<>(RespStatusEnum.FAILURE, CommonEnum.SERVICE_RESPONSE_DATA_ERROR, resp.getData());
			}
		} catch (Exception e) {
			resp = prepareException(e);
		}
		return resp;
	}
	
	/********************************************************************************
	 * 5.健康检查
	 ********************************************************************************/
//	@ApiOperation(value = "健康检查 url", notes = "健康检查 url")
//	@GetMapping(value = "/test-conn")
//	public void testConnection(HttpServletRequest request, HttpServletResponse response) {
//		response.setStatus(200);
//	}
	/********************************************************************************
	 * 
	 ********************************************************************************/
	String proxyPayOrderInputAndPost(ProxyPayBO proxyPayOrder) throws Exception {
		
		//TODO 1.prepare input
		String merchant = channelPayConfig.getMerId(); //商户ID
		String orderNo = proxyPayOrder.getOrderNo(); //商户订单编号，不可重复
		String amount = proxyPayOrder.getTransactionAmount(); //订单金额，保留两位小数
		String accountName = proxyPayOrder.getReceiptAccountName(); //收款人姓名
		String accountNo = proxyPayOrder.getReceiptAccountNumber(); //收款人账号
		String notifyUrl = channelPayConfig.getCallbackProxypayChannelpay(); //異步通知地址 http://294f-211-75-36-190.ngrok.io/jitupay/api/proxy-pay-call-back
		String bankName = proxyPayOrder.getReceiptCardBankName(); //银行名称
		String bankCode ;
		Map<String, String> bankMap = channelPayConfig.getBankChannelMap();
		if(!Optional.fromNullable(bankMap.get(proxyPayOrder.getReceiptCardBankCode())).isPresent()) {
			throw new DiorException(ReturnCodeEnum.BANK_CODE_INVALID).setMsg("Copo银行代码: " + proxyPayOrder.getReceiptCardBankCode()
																			+" 渠道银行代码: " + bankMap.get(proxyPayOrder.getReceiptCardBankCode()));
		}else {
			bankCode = bankMap.get(proxyPayOrder.getReceiptCardBankCode());
		}
		//TODO 2.prepare sign
		//組加簽原串
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("merchant", merchant);
		paramMap.put("orderNo", orderNo);
		paramMap.put("amount", amount);
		paramMap.put("accountName", accountName);
		paramMap.put("accountNo", accountNo);
		paramMap.put("notifyUrl", notifyUrl);
		paramMap.put("bankName", bankName);
		
		String source = SignUtil.getSource(paramMap, channelPayConfig.getMerKey());
		String sign = SignUtil.getSign(source);
		log.info("加签原串:{},加签后字串:{}",source, sign);

		String proxyPayUrl = channelPayConfig.getProxyPayUrl();
		
		JSONObject paramJo = new JSONObject((Map)paramMap);
		paramJo.put("amount", BaseUtil.getDecimal(amount)); 
		JSONObject jo = new JSONObject();
		jo.put("method", "createOrder");
		jo.put("params", paramJo);
		jo.put("sign", sign);
		log.info("代付下单请求地址:{},代付請求參數:{}", proxyPayUrl,jo.toJSONString());
		
		//TODO 3.submit and return receive message
		String js = jo.toJSONString();
		log.info("代付下单请求参数:{}", js); 
//		Data<String> receiveMsg = HttpClientUtils.doPost2(proxyPayUrl, paramMap); //form post
		Data<String> receiveMsg = httpClientUtil.submitJson(proxyPayUrl, js); //submit json
		log.info("receiveMsg: rc:{} data:{}" ,receiveMsg.rc,receiveMsg.data);
		if(receiveMsg.rc == -1) {
			throw new DiorException(ReturnCodeEnum.SERVICE_RESPONSE_DATA_ERROR).setMsg(receiveMsg.data);
		}else if(receiveMsg.rc != 200) {
			throw new DiorException(ReturnCodeEnum.INVALID_STATUS_CODE).setMsg(receiveMsg.rc+ " : " + receiveMsg.data);
		}
		return receiveMsg.data;
	}
	@Resource
	WithdrawOrderMapper withdrawOrderMapper;
	private void insertWithdrawRecord(ProxyPayBO proxyPayBO) {
		String temp = UUID.randomUUID().toString().replaceAll("-", "");
		String Uid = temp.length()>32 ? temp.substring(0,32) : temp; 
		
		WithdrawOrder withdrawOrder = new WithdrawOrder()
		.setWithdrawOrderId(Uid)
		.setWithdrawOrderNo(proxyPayBO.getOrderNo())
		.setReceiptAccountName(proxyPayBO.getReceiptAccountName())//户名
		.setCardBankName(proxyPayBO.getReceiptCardBankName())//银行名称
		.setAccountNumber(proxyPayBO.getReceiptAccountNumber())
		.setTransactionAmount(new BigDecimal(proxyPayBO.getTransactionAmount()))//
		.setCreatedBy(proxyPayBO.getCreatedBy())
		.setCreatedAt(TdDateUtil.getDateTime());
		
		log.info("insertWithdrawRecord:{}",withdrawOrder);
		withdrawOrderMapper.insert(withdrawOrder);
		
	}
	
	
	private String getAvailableChannel() throws Exception {
		
		log.info("取得代付可用通道编码");
		//prepare input
		String merchant = channelPayConfig.getMerId(); //商户ID
		
		//組加簽原串
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("merchant", merchant);
		
		String source = SignUtil.getSource(paramMap, channelPayConfig.getMerKey());
		log.info("加签原串:{}", source);
		String reqSign = SignUtil.getSign(source);
		log.info("加签后字串:{}", reqSign);
//		paramMap.put("sign", sign);

		String proxyPayUrl = channelPayConfig.getProxyPayUrl();
		log.info("下单请求地址:{}", proxyPayUrl);
//		String receiveMsg = HttpClientUtils.doPost(payUrl, paramMap);
		JSONObject paramJo = new JSONObject((Map)paramMap);
		JSONObject jo = new JSONObject();
		jo.put("method", "listChannels");
		jo.put("params", paramJo);
		jo.put("sign", reqSign);
		
		String js = jo.toJSONString();
		log.info("请求参数:{}", js);
		String receiveMsg = HttpClientUtils.submitJson(proxyPayUrl, js);
		log.info("渠道返回讯息:{}", receiveMsg);
		
		//{"result":["trp"],"error":{"code":200,"message":"OK"}}
		JSONObject receiveJO = JSON.parseObject(receiveMsg);
		List<String> resultList = (List<String>) JSONArray.parse(receiveJO.getString("result"));
		
		return resultList.get(0);
	}
	
	/*
	 * Scheduled代付呼叫内部渠道下单，内部渠道服务的Response
		{
		    "status": 0,
		    "code": "000",
		    "msg": "操作成功",
		    "data": {
		        "orderStatus": "",
		        "channelOrderNo": "0d2a8a64-cf3e-4017-af86-922e1c83b802"
		    }
		}
		
		Scheduled判断条件
		#1.status：0=成功、1=失败(失败判定因素，只要是渠道是未成功的都算失败，包含连线异常、余额不足、交易金额错误...等)
		#2.msg：错误原因(当status等于1时，必须填入错误原因)
		备注：当status等于0时，会获取data内的channelOrderNo(可以为空值，但不能无此参数)
	 * */
	
	protected RespData<ProxyPayChannelReplyVO> prepareProxyPayReply(String receiveMsg) throws Exception {

		//TODO 1.处理返回错误讯息
		//ex. error:{"result":null,"error":{"code":-32602,"message":"Invalid params"}}
		//success:{"result":{"name":"trp","isSucceed":true,"before":"295.350",
		//"money":-2,"fees":"1.00","balance":293.35,"msg":"success"},
		//"error":{"code":200,"message":"OK"}}
		
		JSONObject json = ApiUtil.parseToJson(receiveMsg);// 返回JSON字串
		if(json == null) {
			return new RespData<ProxyPayChannelReplyVO>(RespStatusEnum.FAILURE, CommonEnum.CHANNEL_REPLY_ERROR).setExternalMessage(receiveMsg);
		}
		
		int code = json.getInteger("code");	// 200:成功
		String message = json.getString("message");
		if(code != 200) {
			log.info("代付订单 error code:{}, error:{}", code, message);
			return new RespData<ProxyPayChannelReplyVO>(RespStatusEnum.FAILURE, CommonEnum.CHANNEL_REPLY_ERROR).setExternalMessage(message);
		}
		
		//TODO 2.驗簽渠道返回簽名
//		Map<String, String> payDataMap = JSONObject.toJavaObject(json, Map.class);
//		String reqSign = payDataMap.get("sign");
//		payDataMap.remove("sign");
//		
//		if(!SignUtil.signVerify(reqSign, payDataMap, channelpayConfig.getMerKey())) {
//			log.error("验签渠道返回签名出错-sign:{}, paramMap:{}, key:{}", reqSign, payDataMap, channelpayConfig.getMerKey());
//			return new RespData<PayOrderChannelReplyVO>(RespStatusEnum.FAILURE, ReturnCodeEnum.SIGN_ERROR);
//		}
		
		//TODO 3.寫入渠道回傳的訂單相關資訊
		ProxyPayChannelReplyVO replyVO = new ProxyPayChannelReplyVO();
//		replyVO.setChannelOrderNo(transNo);
		
		return new RespData<>(RespStatusEnum.SUCCESS, CommonEnum.SUCCESS, replyVO);
	}
	
	String proxyPayQueryInputAndPost(QueryOrderBO queryOrderBO) throws Exception {

		//TODO 1.prepare input
		String merchant = channelPayConfig.getMerId(); //商户ID
		String orderNo = queryOrderBO.getOrderNo(); //商户订单编号，不可重复
		
		//TODO 2.prepare sign
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("merchant", merchant);
		paramMap.put("orderNo", orderNo);
		
		String source = SignUtil.getSource(paramMap, channelPayConfig.getMerKey());
		String sign = SignUtil.getSign(source);
		log.info("加签原串:{},加签后字串:{}", source,sign);
		
		//TODO 3.submit and return receive message
		JSONObject paramJo = new JSONObject((Map)paramMap);
		JSONObject jo = new JSONObject();
		jo.put("method", "getOrder");
		jo.put("params", paramJo);
		jo.put("sign", sign);
		
		String js = jo.toJSONString();
		log.info("代付查单请求网址:{},代付请求参数:{}",channelPayConfig.getProxyPayQueryUrl(), js);
		Data<String> receiveMsg = httpClientUtil.submitJson(channelPayConfig.getProxyPayQueryUrl(), js);
		log.info("receiveMsg: rc:{} data:{}" ,receiveMsg.rc,receiveMsg.data);
		if(receiveMsg.rc == -1) {
			throw new DiorException(ReturnCodeEnum.SERVICE_RESPONSE_DATA_ERROR).setMsg(receiveMsg.data);
		}else if(receiveMsg.rc != 200) {
			throw new DiorException(ReturnCodeEnum.INVALID_STATUS_CODE).setMsg(receiveMsg.rc+ " : " + receiveMsg.data);
		}
		
		return receiveMsg.data;
	}
	protected RespData<ProxyPayQueryReplyVO> prepareProxyPayQueryReply(String receiveMsg) throws Exception {
		
		//TODO 1.处理返回错误讯息
		//ex. {"result":{"merchant":"copo888","channel":"1","orderNo":"ORD2020102000000002",
		//"amount":1,"payType":1,"accountName":"\u56db\u6bdb","accountNo":"1222222222222",
		//"accountType":1,"mobile":"15605413085","openBankName":"\u6d66\u53d1\u94f6\u884c",
		//"bankName":"\u6d66\u53d1\u94f6\u884c","status":0,"createdTime":"2020-10-21 10:13:09",
		//"notifyUrl":"http:\/\/154.222.0.110:9001\/channelpay\/api\/pay-call-back","notifyContent":"",
		//"notifyMsg":""},"error":{"code":200,"message":"OK"}}
		
		JSONObject json = ApiUtil.parseToJson(receiveMsg);// 返回JSON字串
		if(json == null) {
			return new RespData<ProxyPayQueryReplyVO>(RespStatusEnum.FAILURE, CommonEnum.CHANNEL_REPLY_ERROR).setExternalMessage(receiveMsg);
		}
		
		int code = json.getInteger("code");	// 200:成功
		String message = json.getString("message");
		if(code != 200) {
			log.info("查询代付订单 error code:{}, error:{}", code, message);
			return new RespData<ProxyPayQueryReplyVO>(RespStatusEnum.FAILURE, CommonEnum.CHANNEL_REPLY_ERROR).setExternalMessage(message);
		}

		//TODO 2.驗簽渠道返回簽名
//		Map<String, String> queryDataMap = JSONObject.toJavaObject(json, Map.class);
//		String reqSign = queryDataMap.get("sign");
//		queryDataMap.remove("sign");
//		
//		if(!SignUtil.signVerify(reqSign, queryDataMap, channelpayConfig.getMerKey())) {
//			log.error("验签渠道返回签名出错-sign:{}, paramMap:{}, key:{}", reqSign, queryDataMap, channelpayConfig.getMerKey());
//			return new RespData<QueryOrderChannelReplyVO>(RespStatusEnum.FAILURE, ReturnCodeEnum.SIGN_ERROR);
//		}
		
		//TODO 3.寫入渠道回傳的訂單相關資訊
		ProxyPayQueryReplyVO replyVO = new ProxyPayQueryReplyVO();
		String status = json.getString("status");	//处理中:1 成功:2 失败:4, 5, 6 
		String orderStatus = "1"; //处理中
		if("1".equals(status)) { //成功
			orderStatus = "2";
		}else if("4,5,6".indexOf(status) > -1) { //失败
			orderStatus = "3";
		}
		
		String amount = json.getString("amount");
//		String channelOrderNo = resultJo.getString("serializeId"); //代付中心生成的订单号
		
		replyVO.setChannelReplyDate(TdDateUtil.getDateTime()); //查詢交易日期(渠道若沒回傳需塞現在時間) 
//		replyVO.set(new BigDecimal(amount));
//		replyVO.setChannelOrderNo(channelOrderNo);
		replyVO.setOrderStatus(orderStatus);//订单状态: 状态 0待处理，1处理中，2完成，3失败
		return new RespData<ProxyPayQueryReplyVO>(RespStatusEnum.SUCCESS, CommonEnum.SUCCESS, replyVO);
	}
}