package com.dior.channelpay.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpRequest;
import org.apache.http.client.methods.HttpPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dior.channelpay.config.ChannelPayConfig;
import com.dior.channelpay.util.ApiUtil;
import com.dior.channelpay.util.HttpClientUtil;
import com.dior.channelpay.util.SignUtil;
import com.dior.common.enums.CommonEnum;
import com.dior.common.enums.ReturnCodeEnum;
import com.dior.common.enums.base.BaseEnum;
import com.dior.common.enums.base.RespData;
import com.dior.common.enums.base.RespStatusEnum;
import com.dior.common.exception.DiorException;
import com.dior.common.model.bo.ChannelPayOrder;
import com.dior.common.model.bo.QueryOrderBO;
import com.dior.common.model.vo.ChannelBalanceVO;
import com.dior.common.model.vo.PayCallbackVO;
import com.dior.common.model.vo.PayOrderChannelReplyVO;
import com.dior.common.model.vo.QueryOrderChannelReplyVO;
import com.dior.common.model.vo.ReceiverInfoVO;
import com.dior.common.util.BaseUtil;
import com.dior.common.util.EncryptUtil;
import com.dior.common.util.HttpClientUtils;
import com.dior.common.util.TdDateUtil;
import com.dior.common.util.HttpClientUtils.Data;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("jitupay/api")
@Api(value = "串接 pay 渠道 API")
@Slf4j
public class PayController {

	/********************************************************************************
	 * declare properties
	 ********************************************************************************/
	@Value("${pay.key}")
	private String payKey;

	@Value("${pay.encryptKey}")
	private String payEncryptKey;

	@Value("${spring.application.name}")
	private String channelName;

	@Autowired
	private ChannelPayConfig channelPayConfig;

	@Resource
	private HttpClientUtil httpClientUtil;

	/********************************************************************************
	 * common methods
	 ********************************************************************************/
	boolean authCheck(String sign) {
		log.info("Header 验签 = {}，加密Key：{}，金鑰：{}", sign, payEncryptKey, payKey);
		/** 目前期望加密机制： proxyKey+YYYYMMDDhhmmss， 解密后时间不超过5分钟内 */
		boolean result = EncryptUtil.microServiceDecrypt(sign, payEncryptKey, payKey);
		return result;
	}

	<T> RespData<T> prepareException(Throwable e) {
		return prepareException(e, CommonEnum.CONNECT_SERVICE_FAILURE);
	}

	<T> RespData<T> prepareException(Throwable e, BaseEnum be) {
		log.info("连接渠道出错:{}", e.toString());
		log.info("连接渠道出错LogStack:{}", BaseUtil.getStackString(e));
		if (e instanceof DiorException)
			be = ((DiorException) e).getBaseEnum();

		String msg = e.getMessage();
		if (msg.length() > 500) {
			msg = msg.substring(0, 500) + " ...";
		}
		RespData<T> respData = new RespData<T>();
		respData.setStatus(RespStatusEnum.FAILURE.status);
		respData.setCode(be.getCode()).setMsg(msg);

		return respData;
	}

	/********************************************************************************
	 * 1.串接支付下單渠道
	 ********************************************************************************/
	@ApiOperation(value = "串接 pay 支付下單渠道", notes = "串接支付下單渠道")
	@PostMapping(path = "/pay", produces = { "application/json" })
	public RespData<PayOrderChannelReplyVO> pay(@RequestHeader(name = "authentication-paykey") String sign,
			@Valid @RequestBody ChannelPayOrder channelPayOrder, HttpServletRequest request) {
		RespData<PayOrderChannelReplyVO> resp = null;
		try {
			getIpAddrs(request);
			log.info("支付订单channelName:{},ChannelPayOrder:{}", channelName, channelPayOrder);
			// 1.microService check authentication-paykey
			if (!authCheck(sign))
				return new RespData<>(ReturnCodeEnum.INVALID_SIGN); // 驗簽錯誤

			// 2.prepare input map
			String receiveMsg = payOrderInputAndPost(channelPayOrder);
			log.info("支付订单 receiveMsg:{}", receiveMsg);
			resp = preparePayReply(receiveMsg);
		} catch (Throwable e) {
			resp = prepareException(e);
		}
		return resp;
	}

	/********************************************************************************
	 * 2.串接查询支付订单
	 ********************************************************************************/
	@ApiOperation(value = "串接查询支付订单", notes = "串接查询支付订单")
	@PostMapping(path = "/pay-query", produces = { "application/json" })
	public RespData<QueryOrderChannelReplyVO> orderPayQuery(@RequestHeader(name = "authentication-paykey") String sign,
			@Valid @RequestBody QueryOrderBO queryOrderBO) {
		RespData<QueryOrderChannelReplyVO> resp = null;
		try {
			// 1.microService check authentication-paykey
			log.info("支付订单查询channelName:{},queryOrderBO:{}", queryOrderBO);
			if (!authCheck(sign))
				return new RespData<>(ReturnCodeEnum.INVALID_SIGN); // 驗簽錯誤
			// 2.prepare input map
			String receiveMsg = payQueryInputAndPost(queryOrderBO);
			log.info("支付订单查询 receiveMsg:{}", receiveMsg);
			resp = preparePayQueryReply(receiveMsg);
		} catch (Exception e) {
			resp = prepareException(e);
		}
		return resp;
	}

	@ApiOperation(value = "更改渠道redis資料", notes = "更改渠道redis資料")
	@PostMapping(path = "/resetRedis", produces = { "application/json" })
	public void resetRedis(@RequestHeader(name = "authentication-paykey") String sign, String channelName) {

		try {
			channelPayConfig.resetRedisFromDB();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.info("更改渠道redis資料錯誤:{}", e.getMessage());
			e.printStackTrace();
		}
	}

	/********************************************************************************
	 * 3.串接支付渠道回調
	 ********************************************************************************/
	@ApiOperation(value = "串接支付渠道回调", notes = "串接支付渠道回调")
//	@PostMapping(path = "/pay-call-back", produces = { "application/json" })
//	public String payCallBackAction(@RequestBody Map<String, String> params,HttpServletRequest request) {
//	@PostMapping(path = "/pay-call-back", consumes  = { "multipart/form-data" })
//	public String payCallBackAction(@RequestPart("sh_order") String sh_order ,
//			@RequestPart("pt_order") String pt_order,
//			@RequestPart("money") String money ,
//			@RequestPart("time") String time ,
//			@RequestPart("status") String status ,
//			@RequestPart("remark") String remark ,
//			@RequestPart("sign") String sign ,HttpServletRequest request) {
//	@PostMapping(path = "/pay-call-back", produces = { "application/x-www-form-urlencoded" })
//	public String payCallBackAction(@RequestParam Map<String, String> params, HttpServletRequest request) {
//	@PostMapping(path = "/pay-call-back", produces = { "application/json" })
//	public String payCallBackAction(@RequestBody Map<String, String> params, HttpServletRequest request) {
	@PostMapping(path = "/pay-call-back", produces = { "application/x-www-form-urlencoded" })
	public String payCallBackAction(@RequestParam Map<String, String> params, HttpServletRequest request) {
		try {
			getIpAddrs(request);
			String address = request.getRemoteAddr();
			log.info("串接{},支付渠道回调:{},address:{}", channelName, params, address);
			// 检查渠道回调的白名单
			BaseUtil.checkWhiteList(request, channelPayConfig.getWhiteList());
			return preparePayCallBack(params);
		} catch (Exception e) {
			log.info("串接 {} 支付渠道回調出错:{}", channelName, BaseUtil.getTraceStack(e));
			return "err";
		}

	}

	String preparePayCallBack(Map<String, String> params) throws Exception {
		// 验签
		String reqSign = params.get("sign"); // 簽名
		params.remove("sign");
		if (!SignUtil.signVerify(reqSign, params, channelPayConfig.getMerKey())) {
			log.error("验签出错-sign:{}, paramMap:{}, key:{}", reqSign, params, channelPayConfig.getMerKey());
			return "";
		}
		// ******** Start payCallBackReply ****************
		String orderNo = params.get("order_sn"); // 商戶訂單號
		String serializeId = params.get("sys_order_sn"); // 平台交易单号
		String amount = params.get("pay_money"); // 支付金额
		String status = params.get("pay_state"); // 支付状态： 0未支付,1已支付,2已取消
		String orderStatus = status != null && status.equals("1") ? "1" : "0";
		// ******** End payCallBackReply ****************

		// 呼叫 dior api portal 的回調
		PayCallbackVO callbackVO = new PayCallbackVO();
		callbackVO.setPayOrderNo(orderNo); // 支付訂單號
		callbackVO.setChannelOrderNo(serializeId); // 渠道訂單號 (若无则填入->"CHN_" + orderNo)
		callbackVO.setOrderStatus(orderStatus); // 訂單狀態:成功
		callbackVO.setOrderAmount(new BigDecimal(amount)); // 下單金額
		callbackVO.setCallbackTime(TdDateUtil.getDateTime()); // 回調時間
		String callBackAddress = this.channelPayConfig.getCallbackPayDiorportal();

		// 微服務加密
		HttpPost httpPost = new HttpPost(callBackAddress);
		httpPost.addHeader("authentication-paykey", EncryptUtil.microServiceEncrypt(payKey, payEncryptKey));
		String callBackJson = JSON.toJSONString(callbackVO);
		log.info("回調到diorApiPortal url:{}, json:{}", callBackAddress, callBackJson);
		String respMsg = HttpClientUtils.submitJson(httpPost, callBackJson);
		log.info("回调后资讯:{}", respMsg);
		return "success";
	}

	@ApiOperation(value = "串接pay查询支付余额", notes = "串接pay查询支付余额")
	@PostMapping(path = "/pay-query-balance-internal", produces = { "application/json" })
	public ChannelBalanceVO queryBalanceInternal(@RequestHeader(name = "authentication-paykey") String sign) {
		ChannelBalanceVO resp = queryBalance(sign);
		return resp;
	}

	ChannelBalanceVO queryBalance(String sign) {
		ChannelBalanceVO resp = new ChannelBalanceVO();
		try {
			// microService check authentication-proxykey
			if (!authCheck(sign))
				return resp.setErrorCode("-1").setErrorMsg("验签错误"); // 驗簽錯誤

			// TODO 1.prepare input
			String merchant = channelPayConfig.getMerId(); // 商户ID
			String UUid = BaseUtil.getUUID();
			Map<String, String> paramMap = new HashMap<>();
			paramMap.put("bid", merchant);
			paramMap.put("time", Long.toString(System.currentTimeMillis() / 1000L));

			// TODO 2.prepare sign
			String source = SignUtil.getSource(paramMap, channelPayConfig.getMerKey());
			String reSign = SignUtil.getSign(source);
			paramMap.put("sign", reSign);
			log.info("加签原串:{},加签后字串:{}", source, reSign);

			// TODO 3.submit and get receive message
			log.info("查询余额网址:{}，查询余额请求参数:{}", channelPayConfig.getPayQueryBalanceUrl(), paramMap);
			Data<String> receiveMsg = httpClientUtil.submitForm(channelPayConfig.getPayQueryBalanceUrl(), paramMap);
			log.info("receiveMsg: rc:{} data:{}", receiveMsg.rc, receiveMsg.data);
			if (receiveMsg.rc == -1) {
				throw new DiorException(ReturnCodeEnum.SERVICE_RESPONSE_DATA_ERROR).setMsg(receiveMsg.data);
			} else if (receiveMsg.rc != 200) {
				throw new DiorException(ReturnCodeEnum.INVALID_STATUS_CODE)
						.setMsg(receiveMsg.rc + " : " + receiveMsg.data);
			}

			// TODO 4.处理渠道返回错误讯息
			JSONObject json = ApiUtil.parseToJson(receiveMsg.data);// 返回JSON字串
			if (json == null) {
				log.info("余额查询 error{}:", "receiveMsg.data");
				resp.setErrorCode("-1").setErrorMsg("服务回傳資料錯誤");
			}
			int code = json.getInteger("code"); // 0:成功
			String message = json.getString("msg");
			if (code != 100) {
				log.info("余额查询 error code:{}, error:{}", code, message);
				resp.setErrorCode("-1").setErrorMsg("errorCode: " + code + "errorMsg: " + message);
			}
			JSONObject dataJson = json.getJSONObject("data");
			// TODO 5.返回代付余额
			String balance = dataJson.getString("money");
			log.info("查询余额:{}", balance);
//			replyVO.setDfBalance(BaseUtil.getDecimal(balance));
			resp.setErrorCode("000").setErrorMsg("操作成功");
			resp.setWithdrawBalance(new BigDecimal(balance).setScale(2, BigDecimal.ROUND_DOWN));
			return resp;
		} catch (Exception e) {
			log.error("余额查询错误:{}" + BaseUtil.getTraceStack(e));
		}

		return resp;
	}

	/********************************************************************************
	 * 5.健康检查
	 ********************************************************************************/
	@ApiOperation(value = "健康检查 url", notes = "健康检查 url")
	@GetMapping(value = "/test-conn")
	public void testConnection(HttpServletRequest request, HttpServletResponse response) {
		response.setStatus(200);
	}

	/********************************************************************************
	 * 
	 ********************************************************************************/
	String payOrderInputAndPost(ChannelPayOrder channelPayOrder) throws Exception {
		// 客製化區域 - 1.prepare input
		String merchant = channelPayConfig.getMerId(); // 商户ID
		String orderNo = channelPayOrder.getOrderNo(); // 商户订单编号，不可重复
		String amount = channelPayOrder.getTransactionAmount(); // 订单金额，保留两位小数
		String notify = channelPayConfig.getCallbackPayChannelpay();
//		notify = "http://c7be-211-75-36-190.ngrok.io/jitupay/api/pay-call-back"; // 異步通知地址
		String payType = channelPayConfig.getPayTypeMap().get(channelPayOrder.getPayType()); // 支付類型
		String ip = BaseUtil.getLocalIP(); // 提单者IP

		// 客製化區域 - 2.組加簽原串
		Map<String, String> paramMap = new HashMap<>();
		// ******** Start payInput ****************
		paramMap.put("bid", merchant);
		paramMap.put("money", amount);
		paramMap.put("order_sn", orderNo);
		paramMap.put("notify_url", notify);
		paramMap.put("pay_type", payType);
		// ******** End payInput ****************

		String source = SignUtil.getSource(paramMap, channelPayConfig.getMerKey());
		String sign = SignUtil.getSign(source);
		paramMap.put("sign", sign);
		log.info("加签原串:{},加签后字串:{}", source, sign);

		// 客製化區域 - 3.submit and return receive message
		String payUrl = channelPayConfig.getPayUrl();
		log.info("下单请求地址:{},下单请求参数:{}", payUrl, paramMap);
		// String receiveMsg = HttpClientUtils.doPost(payUrl, paramMap);
		Data<String> receiveMsg = httpClientUtil.submitForm(payUrl, paramMap); // form post
		log.info("receiveMsg: rc:{} data:{}", receiveMsg.rc, receiveMsg.data);
		if (receiveMsg.rc == -1) {
			throw new DiorException(ReturnCodeEnum.SERVICE_RESPONSE_DATA_ERROR).setMsg(receiveMsg.data);
		} else if (receiveMsg.rc != 200) {
			throw new DiorException(ReturnCodeEnum.INVALID_STATUS_CODE).setMsg(receiveMsg.rc + " : " + receiveMsg.data);
		}
		return receiveMsg.data;
	}

	protected RespData<PayOrderChannelReplyVO> preparePayReply(String receiveMsg) throws Exception {

		// 客製化區域 - 1.处理返回错误讯息
		JSONObject json = ApiUtil.parseToJson(receiveMsg);// 返回JSON字串
		if (json == null) {
			return new RespData<PayOrderChannelReplyVO>(RespStatusEnum.FAILURE, CommonEnum.CHANNEL_REPLY_ERROR)
					.setExternalMessage(receiveMsg);
		}
		int code = json.getInteger("code"); // 200:成功
		String message = json.getString("msg");
		if (code != 100) {
			log.info("支付订单 error code:{}, error:{}", code, message);
			return new RespData<PayOrderChannelReplyVO>(RespStatusEnum.FAILURE, CommonEnum.CHANNEL_REPLY_ERROR)
					.setExternalMessage(message);
		}

		PayOrderChannelReplyVO replyVO = new PayOrderChannelReplyVO();
		JSONObject dataJson = json.getJSONObject("data");
		// ******** Start payReply ****************
		String payUrl = dataJson.getString("url");
		;
		// ******** End payReply ****************
		replyVO.setPayPageType("url");
		replyVO.setPayPageInfo(payUrl);
//		replyVO.setOrderAmount(orderAmount);
//		replyVO.setChannelOrderNo(transNo);
		/** 订单状态：状态 0处理中，1成功，2失败 */
		replyVO.setStatus("1");
		return new RespData<>(RespStatusEnum.SUCCESS, CommonEnum.SUCCESS, replyVO);
	}

	String payQueryInputAndPost(QueryOrderBO queryOrderBO) throws Exception {
		// TODO 1.prepare input
		String merchant = channelPayConfig.getMerId(); // 商户ID
		String orderNo = queryOrderBO.getOrderNo(); // 商户订单编号，不可重复

		// TODO 2.prepare sign
		Map<String, String> paramMap = new HashMap<>();
		// ******** Start payQueryInput ****************
		paramMap.put("bid", merchant);
		paramMap.put("order_sn", orderNo);
		// ******** End payQueryInput ****************

		String source = SignUtil.getSource(paramMap, channelPayConfig.getMerKey());
		String sign = SignUtil.getSign(source);
		log.info("加签原串:{},加签后字串:{}", source, sign);
		paramMap.put("sign", sign);
		// TODO 3.submit and return receive message
//		JSONObject paramJo = new JSONObject((Map)paramMap);
//		JSONObject jo = new JSONObject();
//		jo.put("method", "getOrder");
//		jo.put("params", paramJo);
//		jo.put("sign", sign);
//		
//		String js = jo.toJSONString();
		// String receiveMsg =
		// HttpClientUtils.submitJson(channelPayConfig.getPayQueryUrl(), paramMap);
		log.info("查单请求网址:{},查单单请求参数:{}", channelPayConfig.getPayQueryUrl(), paramMap);
		Data<String> receiveMsg = httpClientUtil.submitForm(channelPayConfig.getPayQueryUrl(), paramMap);
		log.info("receiveMsg: rc:{} data:{}", receiveMsg.rc, receiveMsg.data);
		if (receiveMsg.rc == -1) {
			throw new DiorException(ReturnCodeEnum.SERVICE_RESPONSE_DATA_ERROR).setMsg(receiveMsg.data);
		} else if (receiveMsg.rc != 200) {
			throw new DiorException(ReturnCodeEnum.INVALID_STATUS_CODE).setMsg(receiveMsg.rc + " : " + receiveMsg.data);
		}

		return receiveMsg.data;
	}

	protected RespData<QueryOrderChannelReplyVO> preparePayQueryReply(String receiveMsg) throws Exception {

		// TODO 1.处理返回错误讯息
		// ex.
		// receiveMsg:{"result":{"serializeId":2371666,"orderNo":"20201020000000003","amount":"302.00",
		// "reciveAmount":"0.00","rate":0,"merchantAmount":0,"payType":8,"isManual":0,"createdTime":"2020-10-20
		// 13:32:24",
		// "acceptTime":"","product":"","attach":"","notifyUrl":"http:\/\/154.222.0.110:9001\/channelpay\/api\/pay-call-back",
		// "notifyMsg":"","notifyContent":null,"status":0},"error":{"code":200,"message":"OK"}}

		JSONObject json = ApiUtil.parseToJson(receiveMsg); // 返回JSON字串
		if (json == null) {
			return new RespData<QueryOrderChannelReplyVO>(RespStatusEnum.FAILURE, CommonEnum.CHANNEL_REPLY_ERROR)
					.setExternalMessage(receiveMsg);
		}

		int code = json.getInteger("code"); // 200:成功
		String message = json.getString("msg");
		if (code != 100) {
			log.info("支付订单 error code:{}, error:{}", code, message);
			return new RespData<QueryOrderChannelReplyVO>(RespStatusEnum.FAILURE, CommonEnum.CHANNEL_REPLY_ERROR)
					.setExternalMessage(message);
		}

		// TODO 2.驗簽渠道返回簽名
		// ap<String, String> queryDataMap = JSONObject.toJavaObject(json, Map.class);
		// String reqSign = queryDataMap.get("sign");
		// queryDataMap.remove("sign");
		//
		// if(!SignUtil.signVerify(reqSign, queryDataMap, channelpayConfig.getMerKey()))
		// {
		// log.error("验签渠道返回签名出错-sign:{}, paramMap:{}, key:{}", reqSign, queryDataMap,
		// channelpayConfig.getMerKey());
		// return new RespData<QueryOrderChannelReplyVO>(RespStatusEnum.FAILURE,
		// ReturnCodeEnum.SIGN_ERROR);
		// }

		// TODO 3.寫入渠道回傳的訂單相關資訊
		QueryOrderChannelReplyVO replyVO = new QueryOrderChannelReplyVO();
		// ******** Start payQueryReply ****************
		JSONObject dataJson = json.getJSONObject("data");
		String status = dataJson.getString("pay_state"); // 支付状态： 0未支付,1已支付,2已取消
		BigDecimal amount = dataJson.getBigDecimal("pay_money");
		String channelOrderNo = dataJson.getString("sys_order_sn"); // 支付中心生成的订单号
		// ******** End payQueryReply ****************
		String orderStatus = status != null && status.equals("1") ? "1" : "0";

		replyVO.setOrderAmount(amount);
		replyVO.setChannelOrderNo(channelOrderNo);
		replyVO.setOrderStatus(orderStatus);// 订单状态: 状态 0处理中，1成功，2失败
		return new RespData<>(RespStatusEnum.SUCCESS, CommonEnum.SUCCESS, replyVO);
	}

	private static boolean isIpValid(String ip) {
		return !(StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip));
	}

	private static String headerIpFields[] = { "x-forwarded-for", "PRoxy-Client-IP", "WL-Proxy-Client-IP",
			"HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR", "X-Real-IP" };

	public String[] getIpAddrs(HttpServletRequest request) {
		// 正常获取ip地址
		try {
			Map<String, String> map = new HashMap<String, String>();
			String ip = request.getRemoteAddr();
			log.info("getRemoteAddr[{}]\n", ip);
			if (isIpValid(ip)) {
				map.put(ip, ip);
			}
			// 获取代理中的ip地址
			for (String headerIpField : headerIpFields) {
				ip = request.getHeader(headerIpField);
				if (isIpValid(ip)) {
					log.info("Head[{}]=[{}]\n", headerIpField, ip);
					map.put(ip, ip);
				}
			}
			Set<String> keys = map.keySet();
			return keys == null ? new String[0] : keys.toArray(new String[keys.size()]);
		} catch (Exception e) {
		}
		return new String[0];
	}

}